﻿using System;
using System.Collections.Generic;

namespace Projekat.Models
{
    public partial class Turnir
    {
        public int IdTur { get; set; }
        public string NazT { get; set; } = null!;
        public DateTime DatT { get; set; }
        public string MestoT { get; set; } = null!;
        public int IdK { get; set; }

        public virtual TeniskiKlub IdKNavigation { get; set; } = null!;
    }
}
