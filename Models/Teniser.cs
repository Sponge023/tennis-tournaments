﻿using System;
using System.Collections.Generic;

namespace Projekat.Models
{
    public partial class Teniser
    {
        public Teniser()
        {
            MecTenANavigations = new HashSet<Mec>();
            MecTenBNavigations = new HashSet<Mec>();
        }

        public int IdTen { get; set; }
        public string Jmbg { get; set; } = null!;
        public string ImeT { get; set; } = null!;
        public string PrezT { get; set; } = null!;
        public int IdK { get; set; }

        public virtual TeniskiKlub IdKNavigation { get; set; } = null!;
        public virtual ICollection<Mec> MecTenANavigations { get; set; }
        public virtual ICollection<Mec> MecTenBNavigations { get; set; }
    }
}
