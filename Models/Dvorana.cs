﻿using System;
using System.Collections.Generic;

namespace Projekat.Models
{
    public partial class Dvorana
    {
        public Dvorana()
        {
            Mecs = new HashSet<Mec>();
        }

        public int IdD { get; set; }
        public string NazD { get; set; } = null!;
        public string Podloga { get; set; } = null!;

        public virtual ICollection<Mec> Mecs { get; set; }
    }
}
