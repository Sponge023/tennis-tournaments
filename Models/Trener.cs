﻿using System;
using System.Collections.Generic;

namespace Projekat.Models
{
    public partial class Trener
    {
        public int IdTr { get; set; }
        public string ImeTr { get; set; } = null!;
        public string PrezTr { get; set; } = null!;
        public int? IdK { get; set; }

        public virtual TeniskiKlub? IdKNavigation { get; set; }
    }
}
