﻿using System;
using System.Collections.Generic;

namespace Projekat.Models
{
    public partial class Sudija
    {
        public Sudija()
        {
            Mecs = new HashSet<Mec>();
        }

        public int IdS { get; set; }
        public string ImeS { get; set; } = null!;
        public string PrezS { get; set; } = null!;

        public virtual ICollection<Mec> Mecs { get; set; }
    }
}
