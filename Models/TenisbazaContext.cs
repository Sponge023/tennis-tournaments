﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Projekat.Models
{
    public partial class TenisbazaContext : DbContext
    {
        public TenisbazaContext()
        {
        }

        public TenisbazaContext(DbContextOptions<TenisbazaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Dvorana> Dvoranas { get; set; } = null!;
        public virtual DbSet<Mec> Mecs { get; set; } = null!;
        public virtual DbSet<Sudija> Sudijas { get; set; } = null!;
        public virtual DbSet<Teniser> Tenisers { get; set; } = null!;
        public virtual DbSet<TeniskiKlub> TeniskiKlubs { get; set; } = null!;
        public virtual DbSet<Trener> Treners { get; set; } = null!;
        public virtual DbSet<Turnir> Turnirs { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=MARKO\\SQLEXPRESS;Database=Tenisbaza;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Dvorana>(entity =>
            {
                entity.HasKey(e => e.IdD)
                    .HasName("DVORANA_PK");

                entity.ToTable("DVORANA");

                entity.Property(e => e.IdD).HasColumnName("Id_D");

                entity.Property(e => e.NazD)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Naz_D");

                entity.Property(e => e.Podloga)
                    .HasMaxLength(255)
                    .HasDefaultValueSql("('Sljaka')");
            });

            modelBuilder.Entity<Mec>(entity =>
            {
                entity.HasKey(e => e.IdMec)
                    .HasName("MEC_PK");

                entity.ToTable("MEC");

                entity.Property(e => e.IdMec).HasColumnName("Id_Mec");

                entity.Property(e => e.IdD).HasColumnName("Id_D");

                entity.Property(e => e.IdS).HasColumnName("Id_S");

                entity.Property(e => e.Pob)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdDNavigation)
                    .WithMany(p => p.Mecs)
                    .HasForeignKey(d => d.IdD)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MEC_DVO_FK");

                entity.HasOne(d => d.IdSNavigation)
                    .WithMany(p => p.Mecs)
                    .HasForeignKey(d => d.IdS)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MEC_SUD_FK");

                entity.HasOne(d => d.TenANavigation)
                    .WithMany(p => p.MecTenANavigations)
                    .HasForeignKey(d => d.TenA)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MEC_TENA_FK");

                entity.HasOne(d => d.TenBNavigation)
                    .WithMany(p => p.MecTenBNavigations)
                    .HasForeignKey(d => d.TenB)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MEC_TENB_FK");
            });

            modelBuilder.Entity<Sudija>(entity =>
            {
                entity.HasKey(e => e.IdS)
                    .HasName("SUDIJA_PK");

                entity.ToTable("SUDIJA");

                entity.Property(e => e.IdS).HasColumnName("Id_S");

                entity.Property(e => e.ImeS)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Ime_S");

                entity.Property(e => e.PrezS)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Prez_S");
            });

            modelBuilder.Entity<Teniser>(entity =>
            {
                entity.HasKey(e => e.IdTen)
                    .HasName("TENISER_PK");

                entity.ToTable("TENISER");

                entity.HasIndex(e => e.Jmbg, "TENISER_UK")
                    .IsUnique();

                entity.Property(e => e.IdTen).HasColumnName("Id_Ten");

                entity.Property(e => e.IdK).HasColumnName("Id_K");

                entity.Property(e => e.ImeT)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Ime_T");

                entity.Property(e => e.Jmbg)
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.PrezT)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Prez_T");

                entity.HasOne(d => d.IdKNavigation)
                    .WithMany(p => p.Tenisers)
                    .HasForeignKey(d => d.IdK)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TENISER_FK");
            });

            modelBuilder.Entity<TeniskiKlub>(entity =>
            {
                entity.HasKey(e => e.IdK)
                    .HasName("TKLUB_PK");

                entity.ToTable("TENISKI_KLUB");

                entity.Property(e => e.IdK).HasColumnName("Id_K");

                entity.Property(e => e.BrTe).HasColumnName("Br_Te");

                entity.Property(e => e.BrTr).HasColumnName("Br_Tr");

                entity.Property(e => e.ImeK)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Ime_K");
            });

            modelBuilder.Entity<Trener>(entity =>
            {
                entity.HasKey(e => e.IdTr)
                    .HasName("TRENER_PK");

                entity.ToTable("TRENER");

                entity.Property(e => e.IdTr).HasColumnName("Id_Tr");

                entity.Property(e => e.IdK).HasColumnName("Id_K");

                entity.Property(e => e.ImeTr)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Ime_Tr");

                entity.Property(e => e.PrezTr)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Prez_Tr");

                entity.HasOne(d => d.IdKNavigation)
                    .WithMany(p => p.Treners)
                    .HasForeignKey(d => d.IdK)
                    .HasConstraintName("TRENER_FK");
            });

            modelBuilder.Entity<Turnir>(entity =>
            {
                entity.HasKey(e => e.IdTur)
                    .HasName("TURNIR_PK");

                entity.ToTable("TURNIR");

                entity.Property(e => e.IdTur).HasColumnName("Id_Tur");

                entity.Property(e => e.DatT)
                    .HasColumnType("date")
                    .HasColumnName("Dat_T");

                entity.Property(e => e.IdK).HasColumnName("Id_K");

                entity.Property(e => e.MestoT)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Mesto_T");

                entity.Property(e => e.NazT)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("Naz_T");

                entity.HasOne(d => d.IdKNavigation)
                    .WithMany(p => p.Turnirs)
                    .HasForeignKey(d => d.IdK)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TURNIR_FK");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
