﻿using System;
using System.Collections.Generic;

namespace Projekat.Models
{
    public partial class Mec
    {
        public int IdMec { get; set; }
        public int TenA { get; set; }
        public int TenB { get; set; }
        public string? Pob { get; set; }
        public int IdS { get; set; }
        public int IdD { get; set; }

        public virtual Dvorana IdDNavigation { get; set; } = null!;
        public virtual Sudija IdSNavigation { get; set; } = null!;
        public virtual Teniser TenANavigation { get; set; } = null!;
        public virtual Teniser TenBNavigation { get; set; } = null!;
    }
}
