﻿using System;
using System.Collections.Generic;

namespace Projekat.Models
{
    public partial class TeniskiKlub
    {
        public TeniskiKlub()
        {
            Tenisers = new HashSet<Teniser>();
            Treners = new HashSet<Trener>();
            Turnirs = new HashSet<Turnir>();
        }

        public int IdK { get; set; }
        public string ImeK { get; set; } = null!;
        public int? BrTr { get; set; }
        public int? BrTe { get; set; }

        public virtual ICollection<Teniser> Tenisers { get; set; }
        public virtual ICollection<Trener> Treners { get; set; }
        public virtual ICollection<Turnir> Turnirs { get; set; }
    }
}
