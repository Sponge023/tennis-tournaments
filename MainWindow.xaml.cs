﻿using Microsoft.EntityFrameworkCore;
using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private TenisbazaContext teniscontext = new TenisbazaContext();

        private CollectionViewSource teniserViewSource;
        private CollectionViewSource trenerViewSource;
        private CollectionViewSource sudijaViewSource;
        private CollectionViewSource mecViewSource;
        private CollectionViewSource dvoranaViewSource;
        private CollectionViewSource turnirViewSource;
        private CollectionViewSource teniskiklubViewSource;

        public MainWindow()
        {
            InitializeComponent();


            teniserViewSource = (CollectionViewSource)FindResource(nameof(teniserViewSource));
            trenerViewSource = (CollectionViewSource)FindResource(nameof(trenerViewSource));
            sudijaViewSource = (CollectionViewSource)FindResource(nameof(sudijaViewSource));
            mecViewSource = (CollectionViewSource)FindResource(nameof(mecViewSource));
            dvoranaViewSource = (CollectionViewSource)FindResource(nameof(dvoranaViewSource));
            turnirViewSource = (CollectionViewSource)FindResource(nameof(turnirViewSource));
            teniskiklubViewSource = (CollectionViewSource)FindResource(nameof(teniskiklubViewSource));
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            teniscontext.Tenisers.Load();
            teniscontext.Treners.Load();
            teniscontext.Sudijas.Load();
            teniscontext.Mecs.Load();
            teniscontext.Dvoranas.Load();
            teniscontext.Turnirs.Load();
            teniscontext.TeniskiKlubs.Load();

            teniserViewSource.Source = teniscontext.Tenisers.Local.ToObservableCollection();
            trenerViewSource.Source = teniscontext.Treners.Local.ToObservableCollection();
            sudijaViewSource.Source = teniscontext.Sudijas.Local.ToObservableCollection();
            mecViewSource.Source = teniscontext.Mecs.Local.ToObservableCollection();
            dvoranaViewSource.Source = teniscontext.Dvoranas.Local.ToObservableCollection();
            turnirViewSource.Source = teniscontext.Turnirs.Local.ToObservableCollection();
            teniskiklubViewSource.Source = teniscontext.TeniskiKlubs.Local.ToObservableCollection();

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            teniscontext.Dispose();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int n = teniscontext.SaveChanges();

            TeniserDataGrid.Items.Refresh();
            SudijaDataGrid.Items.Refresh();
            TrenerDataGrid.Items.Refresh();
            MecDataGrid.Items.Refresh();
            DvoranaDataGrid.Items.Refresh();
            TurnirDataGrid.Items.Refresh();

            MessageBox.Show("Broj izvršenih zapisa: " + n, "Snimanje");
        }
    }
}
